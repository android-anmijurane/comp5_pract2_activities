package com.anmijurane.c5_pact2_activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText txtName;
    private Button btnAcept;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtName = (EditText) findViewById(R.id.txtName);
        btnAcept = (Button) findViewById(R.id.btnAcept);

        btnAcept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        SaludoActivity.class);

                Bundle bd = new Bundle();
                bd.putString("NOMBRE", txtName.getText().toString());

                intent.putExtras(bd);

                startActivity(intent);
            }
        });

    }


}