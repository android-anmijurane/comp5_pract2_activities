package com.anmijurane.c5_pact2_activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class SaludoActivity extends AppCompatActivity {

    private TextView txtHello;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saludo);

    txtHello = (TextView) findViewById(R.id.txtSaludo);

    Bundle bundle = this.getIntent().getExtras();

    txtHello.setText("Hola " + bundle.getString("NOMBRE"));

    }

}